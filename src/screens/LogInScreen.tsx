import React, {FormEvent, useState} from 'react';
import Article from "../components/Article";
import {makeStyles} from "@material-ui/core";
import {Link, TextField} from "@mui/material";
import MyPageScreen from "./MyPageScreen";
import {useHistory} from "react-router-dom";
import {useKeycloak} from "@react-keycloak/web";



const useStyles = makeStyles(() => ({
    title: {
        textAlign: 'center',
        fontSize: '30px',
        fontWeight: 'bold',
        marginTop: '12px',
        marginBottom: '12px'
    },

    loginBody: {
        display: 'flex',
        backgroundColor: '#00005A',
        justifyContent: 'center',
        "@media (min-width: 650px)": {
           width:"500px",
            marginLeft:"auto",
            marginRight:"auto",

        },
        "@media (max-width: 650px)": {
           maxWidth:"90%",
            margin: "0 auto"
        }

    },
    loginForm: {
        display: 'flex',
        flexDirection: 'column',
        width: '90%',
        marginRight: '50px',
        marginLeft: '10px',
        marginTop: '40px',
        marginBottom: '40px'
    },

    usernameSection: {
        marginBottom: '40px'
    },

    passwordSection: {
        marginBottom: '40px'
    },

    textLabel: {
        color: 'white',
        marginBottom: '7px'
    },
    input: {

        height: '50px',
        width: '100%',
        backgroundColor: 'white',
    },
    bottomItem: {},
    buttonSubmit: {
        backgroundColor: '#238636',
        width: '100%',
        height: '50px',
        fontSize: '20px',
        fontWeight: 'bold',
    },
    forgot: {
        textAlign: 'end',
        marginTop: '27px',
        fontStyle: '#7D18FD',

    },
    articleContainer:{
        display:"flex",
        "@media (max-width: 650px)": {
            maxWidth: "95%",
            flexDirection:"column",
            justifyContent:"center",
            margin: "0 auto"
        },
        "@media (min-width: 650px)": {
            flexWrap: "wrap",
            justifyContent:"center",
        }
    }


}));


const LogInScreen:React.FC= () => {
    const classes = useStyles();
    const history = useHistory()
    const [email, setEmail] = React.useState<string>('');
    const [password, setPassword] = React.useState<string>('');

    const [isLogin, setIsLogin] = React.useState<boolean>(false);
    const {keycloak, initialized} = useKeycloak()
    const kcToken = keycloak?.token ?? '';



    const routSubmit = () => {
        let path = "/student/myHome"
        history.push(path)
    }

    /*    const onChangeHandler = (e:React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
       dataStore.email = e.target.value
          //  dataStore.password = e.target.value

        };*/

     /*   const checkStudent = (e:FormEvent<HTMLFormElement>) => {
            e.preventDefault()
            console.log("tokenn", kcToken);
            if (kcToken) {
            fetch(`http://localhost:8080/user/login?email=${email}&password=${password}`,{
                headers: {
                    "Authorization": "Bearer " + kcToken
                }
            })
                .then(repo => repo.json())
                .then(res => {
                    setIsLogin(res)
                    console.log(kcToken)
                })

            }
            if(isLogin){
                routSubmit()
            }

        };*/


        return (

        <div>
            <div className={classes.title}>Log in</div>
            <div className={classes.loginBody}>
                <form className={classes.loginForm} >{/*onSubmit={checkStudent}*/}
                    <div className={classes.usernameSection}>
                        <div className={classes.textLabel}>Username or email address</div>
                        <TextField
                            className={classes.input}
                            id="username"
                            placeholder='Username...'
                            type="text"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            //onChange={onChangeHandler}
                        />
                    </div>

                    <div className={classes.passwordSection}>
                        <div className={classes.textLabel}>Enter Password</div>
                        <TextField
                            className={classes.input}
                            id="password"
                            type="password"
                            placeholder='Password...'
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>

                    <div className={classes.bottomItem}>
                        <button  type='submit'
                            className={classes.buttonSubmit}
                                 data-testid="singIn"
                        >Sign In</button>
                    </div>
                    <div className={classes.forgot}>
                        <Link style={{color:'#7D18FD'}} href="#">forgot password?</Link>
                    </div>
                </form>
            </div>
            <div className={classes.articleContainer}>
                <Article titleText={"Så hittar du lia"} text={"Guide för hur du hittar lia"}/>
                <Article titleText={"Skapa CV"} text={"Guide för hur du skapar CV"}/>
                <Article titleText={"Arbeta i Sverige"}
                         text={"Rättigheter, skyldigheter, lön och förmåner – hur fungerar det?"}/>
            </div>
        </div>
    );
};
export default LogInScreen
/*
export default LogInScreen;
    const checkStudent = (e:FormEvent<HTMLFormElement>) => {
        e.preventDefault()
            fetch(`http://localhost:8080/user/login?email=${email}&password=${password}`)
                .then(repo => repo.json())
                .then(res => {
                    setIsLogin(res)
                    console.log(res)
                })
            if(isLogin){
                routSubmit()
            }

        };*/
