import React from 'react';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import LandingScreen from "../LandingScreen";


test('test_button-intresseanmälan', () => {
    render(<LandingScreen/>);
    const menuBtn = screen.getByText("Intresseanmälan")
    expect(menuBtn).toBeVisible();
    expect(menuBtn).toBeInTheDocument();
    expect(menuBtn).toBeEnabled();
});

test('test_logo-landing', () => {
    render(<LandingScreen/>);
    const linkElement = screen.getByTestId("logoImg");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});


