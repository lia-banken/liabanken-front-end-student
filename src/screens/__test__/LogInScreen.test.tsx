import React from 'react';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import LogInScreen from "../LogInScreen";


test('test_text_LoggaIn', () => {
    render(<LogInScreen/>);
    const linkElement = screen.getByText("Log in");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});

test('test_SignInButton', () => {
    render(<LogInScreen/>);
    const menuBtn = screen.getByTestId("singIn")
    expect(menuBtn).toBeVisible();
    expect(menuBtn).toBeInTheDocument();
    expect(menuBtn).toBeEnabled();
});

test('test_userName_placeholder', () => {
    render(<LogInScreen/>);
    const linkElement = screen.getByPlaceholderText("Username...");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});

