import React, {useEffect, useState} from 'react';
import {Button, makeStyles} from "@material-ui/core";
import {TextField} from "@mui/material";
import DropDownOrt from "../components/allAdvertListComponents/DropDownOrt";
import DropDownYrke from "../components/allAdvertListComponents/DropDownYrke";
import {useHistory} from "react-router-dom";
import SearchIcon from '@mui/icons-material/Search';
import FavoriteList from "../components/profile/FavoriteList";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {IAdvertItems} from "../interface";
import AdvertListComponent from "../components/allAdvertListComponents/AdvertListComponent";
//import {API} from '../router/Api';
import SelectAdvertScreen from "./SelectAdvertScreen";


const useStyles = makeStyles(() => ({
    listBody: {
        display: 'flex',
        flexDirection: "column",
        justifyContent: 'center',
    },
    goBackButton: {
        display: "flex",
        justifyContent: "left",

    },
    allBlock: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: "center",
        width: '85%',
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: '40px',
        marginBottom: '40px'
    },
    title1: {
        fontSize: '20px',
        marginBottom: '40px'
    },
    title2: {
        fontSize: '20px',
        marginBottom: '15px'
    },

    searchBlock: {
        marginBottom: '25px',
        display: "flex",
        width: "100%",
        flexDirection: "column",
        justifyContent: "center",

    },
    input: {
        width: "80%",
        backgroundColor: 'white',
        border: 'none'
    },
    filterAction: {
        display: 'flex',
        justifyContent: 'space-evenly',
        "@media (max-width: 500px)": {
            flexDirection: "column"
        }
    },
    searchButton: {
        height: '55px',
        backgroundColor: "#081992",
        color: "#efeaea",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#4c5499",
        },
        "@media (min-width: 800px)": {
            marginLeft: '0.5%'
        }
    },
    advertContainer: {
        display: "flex",
        width: "90%",
        justifyContent: "center",

    }
}));


const AdvertListScreen = () => {
    const classes = useStyles()
    const history = useHistory();
    const [annonser, setAnnons] = useState<IAdvertItems[]>([])
    const [selectedAnnons, setSelectedAnnons] = useState<IAdvertItems>()
    const [icon, setIcon] = useState("NOT FAV");
    const [checked, setChecked] = useState(false);


//get
    useEffect(() => {
        const fetchData = async () => {
            await fetch("http://localhost:8080/student/all-adverts/")
                .then(resp => resp.json())
                .then(annons => setAnnons(annons))
        };

        const timer = setTimeout(() => {
            fetchData();
        }, 1000);

        return () => clearTimeout(timer);
    }, []);


    /*    console.log(annonser)
        useEffect(() => {
            return () => {
                fetch("http://localhost:8080/annons")
                    .then(resp => resp.json())
                    .then(annons => setAnnons(annons))
            };
        }, []);*/

    /* useEffect(() => {
         const timer = setTimeout(() => {
             console.log('This will run after 1 second!')
         }, 1000);
         return () => clearTimeout(timer);
     }, []);
 */

    const toggleHandler = (id: string) => {
        fetch(`http://localhost:8080/student/add-favorite/${id}`, {
            method: "PUT",
            headers: {
                Accept: "*/*",
            }
        })
            .then(resp => resp.json())
            .then(res => {
                setAnnons(res)
                console.log(res)
            })

    }



    const btnHandler = (item: IAdvertItems) => {
        console.log("itemis" + item)
        //history.push("advert/" + item.id)
   /*     API.fetchGetSelectedList(item.id)
            .then(data => data.json())
            .then(listen => setSelectedAnnons(listen))
            .finally(() => history.push("advert/" + item.id))*/

    }


    return (
        <div>
            <div className={classes.listBody}>
                <div className={classes.allBlock}>
                    <Button className={classes.goBackButton}
                            onClick={() => history.push("/student/homePage")}><ArrowBackIcon/> </Button>
                    <div className={classes.searchBlock}>
                        <div>
                            <h4 className={classes.title2}>Sök på yrke eller ort</h4>
                        </div>
                        <div>
                            <TextField
                                className={classes.input}
                                id="search"
                                placeholder='Skriv in text...'
                                type="text"
                            />
                            <Button variant={'outlined'} className={classes.searchButton}>
                                <SearchIcon/>
                                Sök</Button>
                        </div>
                    </div>
                    <div className={classes.filterAction}>
                        <DropDownOrt/>
                        <DropDownYrke/>
                    </div>
                </div>
            </div>
            <div className={classes.advertContainer}>
                <AdvertListComponent
                    btnGetOneAnons={btnHandler}
                    onToggle={toggleHandler}
                    list={annonser}
                />
                <div hidden={false}>
                    <SelectAdvertScreen data={selectedAnnons}/>

                </div>
            </div>
        </div>
    );
};

export default AdvertListScreen;
