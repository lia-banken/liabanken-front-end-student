import React, {useState} from 'react';
import {Box, Button, Divider, TextField} from "@mui/material";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    inputContainer: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        '& .MuiTextField-root': {marginTop: "20px", width: "300px"}
    },
    btn: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        '& 	.MuiButton-root': {marginLeft: "20px", marginRight: "20px", marginTop: "50px", marginBottom: "50px"}
    }
}))

const RegistrationOfInterestScreen = () => {
    const [data, setData] = useState()
    const dataStore = ({
        name:'',
        email:'',
        tel:'',
        orgNum:'',
        postAdress:'' ,
        postNum:'',
        postOrt:'',
        webSida:'',
        land:'',
        skolan:'',
        företag:''
    });

    const onChangeHandler = (e:React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>)=> {
        switch (e.target.name) {
            case 'name':  dataStore.name = e.target.value
                break
            case 'email':  dataStore.email = e.target.value
                break
            case 'tel':  dataStore.tel = e.target.value
                break
            case 'orgNum':  dataStore.orgNum = e.target.value
                break
            case 'postAdress':  dataStore.postAdress = e.target.value
                break
            case 'postNum':  dataStore.postNum = e.target.value
                break
            case 'postOrt':  dataStore.postOrt = e.target.value
                break
            case 'webSite':  dataStore.webSida = e.target.value
                break
            case 'land':  dataStore.land = e.target.value
                break
            case 'skolan':  dataStore.skolan = e.target.value
                break
            case 'företag':  dataStore.företag = e.target.value
                break
        }
    }

    const classes = useStyles();

    return (
        <div>
            <h3 style={{textAlign: "center"}}>Intresseanmälan</h3>
            <div style={{
                backgroundColor: "#EFEFEF",
                display: "flex",
                flexDirection: "column",
                alignItems: "center"
            }}>
                <Box
                    sx={{boxShadow: 3}}
                    style={{
                        backgroundColor: "white",
                        width: "90%",
                        margin: "20px"
                    }}>
                    <p style={{fontWeight: "bold", textAlign: "center"}}>Fyll i uppgifterna till din profil</p>
                    <div className={classes.inputContainer}>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='name' label="Namn (obligatorisk)" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='email' label="Email (obligatorisk)" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='tel' label="Telefonnummer" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='orgNum' label="Organisationsnummer(obligatorsk)" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='postAdress' label="Postadress" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='postNum' label="Postnummer (obligatorisk)" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='postOrt' label="Postort (obligatorisk)" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='webSite' label="Websida" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='land' label="Land" variant="outlined"/>
                    </div>
                    <Divider style={{marginTop: "20px"}}/>
                    <div className={classes.inputContainer}>
                        <p>Kontaktperson</p>
                        <TextField id="outlined-basic" label="Namn (obligatorisk)" variant="outlined"/>
                        <TextField id="outlined-basic" label="Telefonnummer (obligatorisk)" variant="outlined"/>
                        <TextField id="outlined-basic" label="Email (obligatorisk)" variant="outlined"/>
                    </div>
                    <Divider style={{marginTop: "20px"}}/>
                    <div className={classes.inputContainer}>
                        <p>Typ av verksamhet (obligatorisk)</p>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='skolan' label="Skola" variant="outlined"/>
                        <TextField onChange={onChangeHandler} id="outlined-basic" name='företag' label="Företag" variant="outlined"/>
                    </div>
                    <div className={classes.btn}>
                        <Button variant={"contained"}>Skicka intresseanmälan</Button>
                        <Button variant={"outlined"} color="error">Avbryt</Button>
                    </div>
                </Box>
            </div>
        </div>
    );
};

export default RegistrationOfInterestScreen;
