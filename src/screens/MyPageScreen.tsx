import React from 'react';
import {useHistory} from "react-router-dom";
import {makeStyles} from "@material-ui/core";
import MenuDropdown from "../components/profile/MenuDropdown";
import MyHome from "../components/profile/views/MyHome";
import MyDetails from "../components/profile/views/MyDetails";
import MyFavorites from "../components/profile/views/MyFavorites";
import MyPresentation from "../components/profile/views/MyPresentation";
import Settings from "../components/profile/views/Settings";
import {Button} from "@mui/material";

const useStyles = makeStyles(() => ({
    heading: {
        textAlign: "center",
        margin: "80px",
    },
    menuRouteContainer: {
        backgroundColor: "#EFEFEF",
        paddingBottom: "20px",
        paddingTop: "20px",
        display: "flex",
        flexDirection: "column",
    },
    myHome: {
        '@media (min-width: 1000px)': {
            display: "flex",
            flexDirection: "row",
            margin: "0 auto",
            alignItems: "center",
            paddingTop: "200px",
            paddingBottom: "200px",
            width: "50%"
        }
    },
    myDetails: {
    },
    myPresentation: {
        display: "flex",
        flexDirection: "column",
        border: "solid",
        alignItems: "center",
        width: "100%"
    },
    myFavorites: {},
    settings: {}
}));

const
    MyPageScreen = () => {
    const history = useHistory()
    const classes = useStyles();

    /*const routSubmit = () => {
        let path = "/school/listPage"
        history.push(path)
    }*/
    const routeView = () => {
        switch (history.location.pathname) {
            case "/student/myHome":
                return <div
                    className={classes.myHome}>
                    <MenuDropdown/>
                    <MyHome/>
                </div>
            case "/student/myInfo":
                return <div
                    className={classes.myDetails}>
                    <MenuDropdown/>
                    <MyDetails/>
                </div>
            case "/student/myPresentation":
                return <div
                    className={classes.myPresentation}>
                    <MenuDropdown/>
                    <MyPresentation/>
                </div>
            case "/student/myFavorite":
                return <div className={classes.myFavorites}>
                    <MenuDropdown/>
                    <MyFavorites/>
                </div>
            case "/student/settings":
                return <div className={classes.settings}>
                    <MenuDropdown/>
                    <Settings/>
                </div>
        }
    }

    return (
        <div>
            <h2 className={classes.heading}>Välkommen till mina sidor</h2>
            <div className={classes.menuRouteContainer}>
                {routeView()}
            </div>

        </div>
    );
};

export default MyPageScreen;
