import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core';
import HeaderButton from "./HeaderButton";
import {Link} from "@mui/material";
import DrawerMenu from "./DrawerMenu";
import {useKeycloak} from "@react-keycloak/web";

const useStyles = makeStyles(() => ({
    headerContainer:{
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-evenly",
        alignItems:"center",
        maxWidth:"100%",
        borderBottom:"1px solid lightgrey",
        marginBottom:"1%",
        "@media (max-width: 800px)":{
            paddingRight:"2%",
            paddingLeft:"2%",
            height:"50px",
        },
        "@media (min-width: 800px)":{
            paddingRight:"1%",
            paddingLeft:"1%",
            height:"60px",
        }
    },
    headerButtons:{
        display:"flex",
        flexDirection:"row",
        justifyContent:"end",
        width:"90%",
    }
}));

const Header: React.FC = () => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const {keycloak, initialized} = useKeycloak()
    const btnHandler = () => {
        setIsOpen(true)
    };




    return (
        <div className={classes.headerContainer}>
            <Link href="/" data-test='header-link' underline="none" style={{fontSize: "20px", fontWeight: "bold"}}>
                LiaBanken
            </Link>
            <div className={classes.headerButtons} data-test='header-second-child' >
                {keycloak.authenticated ?
                    <HeaderButton text={"Logga out"} onClick={() => keycloak.logout()} rout={"/student/login"} /> :
                    <HeaderButton text={"Logga In"} onClick={() => keycloak.login()}/>
                }

                <HeaderButton text={"Sök"} rout={"/student/listPage"}/>
                <HeaderButton text={"Menu"} onClick={() => btnHandler()}>Menu</HeaderButton>
            </div>
            <DrawerMenu isOpen={isOpen} setIsOpen={setIsOpen}/>
        </div>
    );
};

export default Header;
