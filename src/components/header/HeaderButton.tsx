import React from 'react';
import {makeStyles} from "@material-ui/core";
import {useHistory} from 'react-router-dom'
import {Button} from "@mui/material";

const useStyles = makeStyles(() => ({
    headerButton: {
        margin: "5px",
        textTransform: "none",

    }
}));

interface IHeaderButton {
    text: string;
    rout?: string;
    onClick?: () => void;
}

const HeaderButton: React.FC<IHeaderButton> = (props) => {
    const classes = useStyles();
    const history = useHistory()
    return (
        <div className={classes.headerButton} data-testid="view-text-header-button">
            <Button
                    variant="contained"
                    href={props.rout}
                    onClick={props.onClick}
            >
                {props.text}
            </Button>
        </div>
    );
};

export default HeaderButton;
