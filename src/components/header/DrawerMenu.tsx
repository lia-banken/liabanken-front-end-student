import React, {FC} from 'react';
import Drawer from '@mui/material/Drawer';
import DrawerList from "./DrawerList";
import {makeStyles} from "@material-ui/core";


const useStyles = makeStyles({
    container: {},
});

interface iDrawerMenu {
    isOpen: boolean,
    setIsOpen: (value: boolean) => void
}

const DrawerMenu: FC<iDrawerMenu> = (props) => {
    const styles = useStyles();

    const toggleDrawer = (open: boolean) => {
        props.setIsOpen(open);
    }

    return (
        <div className={styles.container}>
            <React.Fragment>
                <Drawer
                    anchor={"right"}
                    open={props.isOpen}
                    onClose={() => toggleDrawer(false)}
                >
                    <DrawerList isOpen={props.setIsOpen}/>
                </Drawer>
            </React.Fragment>
        </div>
    );
}

export default DrawerMenu;
