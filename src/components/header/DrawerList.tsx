import React, {FC} from 'react'
import {makeStyles} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import ListItemIcon from "@mui/material/ListItemIcon";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import HomeIcon from '@mui/icons-material/Home';
import LoginIcon from '@mui/icons-material/Login';
import SearchIcon from '@mui/icons-material/Search';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import WorkIcon from '@mui/icons-material/Work';
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';
import ModelTrainingIcon from '@mui/icons-material/ModelTraining';
import WorkspacesIcon from '@mui/icons-material/Workspaces';
import InfoIcon from '@mui/icons-material/Info';
import ContactSupportIcon from '@mui/icons-material/ContactSupport';
import LanguageIcon from '@mui/icons-material/Language';

const useStyles = makeStyles({
    container: {
        width: "200px",
        height: "100px",
        '@media(min-width: 1000px)': {width: "400px"}
    },
    listFooter: {border: "2px solid white"},
    listText: {fontSize: "15px"},
});

interface IDrawerList {
    isOpen: (value: boolean) => void
}

const DrawerList: FC<IDrawerList> = ({isOpen}) => {
    const history = useHistory();
    const classes = useStyles();

    const itemClickedRouting = (text: string) => {
        isOpen(false)
        switch (text) {
            case "Start":
                return history.push("/")
            case "Mina sidor": {
                history.push("/student/myHome")
                return window.location.reload();
            }
            case "Sök Praktik":
                return history.push("/student/listPage")
            case "Logga in":
                return history.push("/student/login")
            case "CV, Intervju":
                return history.push("/student/profile")
            case "Extra Stöd":
                return history.push("/student/profile")
            case "Yrken och framtid":
                return history.push("/student/profile")
            case "Utbildning och studier":
                return history.push("/student/profile")
            case "Arbeta i Sverige":
                return history.push("/student/profile")
        }
    }

    const iconGenerator = (text: string) => {
        switch (text) {
            case "Start":
                return <DoubleArrowIcon/>
            case "Mina sidor":
                return <HomeIcon/>
            case "Sök Praktik":
                return <SearchIcon/>
            case "Logga in":
                return <LoginIcon/>
            case "CV, Intervju":
                return <MeetingRoomIcon/>
            case "Extra Stöd":
                return <AccessibilityIcon/>
            case "Yrken och framtid":
                return <WorkIcon/>
            case "Utbildning och studier":
                return <ModelTrainingIcon/>
            case "Arbeta i Sverige":
                return <WorkspacesIcon/>
            case "Om oss":
                return <InfoIcon/>
            case "Kontakta Oss":
                return <ContactSupportIcon/>
            case "Other Languages":
                return <LanguageIcon/>
        }
    }

    return (
        <div className={classes.container}>
            <List style={{borderBottom: "2px solid lightgrey", borderBottomWidth: "20px", paddingBottom: "0px"}}>
                {['Start', 'Mina sidor', 'Sök Praktik'].map((text, index) => (
                    <ListItem
                        button key={text}
                        onClick={() => itemClickedRouting(text)}
                        style={{borderBottom: "2px solid lightgrey", marginTop: "5px", paddingRight: "5px"}}
                        href={"/login"}
                    >
                        <ListItemIcon>
                            {iconGenerator(text)}
                        </ListItemIcon>
                        <p className={classes.listText}>
                            {text}
                        </p>
                    </ListItem>
                ))}
            </List>
            <List className={classes.listFooter}>
                {['Logga in', 'CV, Intervju', 'Extra Stöd', 'Yrken och framtid', "Utbildning och studier",
                    "Arbeta i Sverige", "Om oss", "Kontakta Oss", "Other Languages"].map((text, index) => (
                    <ListItem
                        button key={text}
                        onClick={() => itemClickedRouting(text)}
                        style={{borderBottom: "2px solid lightgrey", marginTop: "5px", paddingRight: "5px"}}
                    >
                        <ListItemIcon>
                            {iconGenerator(text)}
                        </ListItemIcon>
                        <p className={classes.listText}>
                            {text}
                        </p>
                    </ListItem>
                ))}
            </List>
        </div>
    )
}

export default DrawerList;
