import React from 'react';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';



import HeaderButton from "../HeaderButton";
import Header from "../Header";
import * as ReactDOM from "react-dom";


describe('Header component tests', () => {
    let container: HTMLDivElement
    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container)
        ReactDOM.render(<Header />, container)
    })

    afterEach(() => {
        document.body.removeChild(container);
        container.remove();
    })

    test('test render Header correct', () => {
        expect(container.firstElementChild).toBeInTheDocument();
        expect(container.firstElementChild).toContainHTML("LiaBanken");
    });
    test('test render Header incorrect', () => {
        expect(container.firstElementChild).toBeInTheDocument();
        expect(container.firstElementChild).not.toContainHTML("Liabanken");
    });

})