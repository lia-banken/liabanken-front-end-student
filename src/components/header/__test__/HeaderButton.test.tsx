import React from 'react';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import * as ReactDOM from "react-dom";

import '@testing-library/jest-dom';

import HeaderButton from "../HeaderButton";


describe('HeaderButton component tests', () => {
    let container: HTMLDivElement
    let text:string = "HEJ";
    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container)
        ReactDOM.render(<HeaderButton text={text} rout={""} />, container)
    })

    afterEach(() => {
        document.body.removeChild(container);
        container.remove();
    })

    test('test render HeaderButton correct', () => {
        expect(container.firstElementChild).toBeInTheDocument();
        expect(container.firstElementChild).toContainHTML("HEJ");
    });
    test('test render HeaderButton incorrect', () => {
        expect(container.firstElementChild).toBeInTheDocument();
        expect(container.firstElementChild).not.toContainHTML("HELLO");
    });
})