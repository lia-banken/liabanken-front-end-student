import React from 'react';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Header from "../header/Header";



test('test_button_LoggaIn', () => {
    render(<Header/>);
    const linkElement = screen.getByText("LiaBanken");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});



