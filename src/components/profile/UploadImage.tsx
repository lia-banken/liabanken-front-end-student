import React, {FC} from 'react';
import { Button} from "@mui/material";
import profileImage from "../../assets/chat.jpg";
import {Avatar, makeStyles} from "@material-ui/core";
import {styled} from "@mui/material/styles";

const useStyles = makeStyles(() => ({
    container: {
     /*   border: "2px solid black",
        paddingTop: "10px",
        paddingBottom: "20px",



        '& p': {fontWeight: "bold", marginLeft: "10px"},
           '& .MuiButton-root': {marginTop:'10px'}
*/

    },
    title:{
        marginLeft:'15px',
        fontWeight:'bold'
    },
    imageContainer: {
     /*   border:'1px solid',
        width:'50%',

        display: "flex",
        flexDirection: "column",
        marginRight:'100px',
        alignItems: "center"*/
    },
    image: {
        width: "100px",
        height: "100px",

    }
}))

const Input = styled('input')({
    display: 'none',
});

interface I{
    title: string,
    btnTitle: string
}

const UploadImage:FC<I> = (props) => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <p className={classes.title}>{props.title}</p>
            <div style={{display: 'flex', flexDirection: 'column',justifyContent: 'start', marginLeft:'20px'}}>
                <div style={{marginLeft:'5px', marginBottom:'6px'}}>
                    <Avatar
                        alt="Remy Sharp"
                        src={profileImage}
                        className={classes.image}
                    />
                </div>
                <div>
                    <label htmlFor="contained-button-file">
                        <Input accept="image/*" id="contained-button-file" multiple type="file"/>
                        <Button  variant="contained" component="span">
                            {props.btnTitle}
                        </Button>
                    </label>
                </div>
            </div>
        </div>
    );
};

export default UploadImage;
