import React, {ChangeEvent, useState} from 'react';
import {IAdvertItems} from "../../interface";
import {Checkbox, makeStyles} from "@material-ui/core";
import {Favorite, FavoriteBorder} from "@mui/icons-material";
import {Button} from "@mui/material";


const useStyles = makeStyles((theme) => ({
    favoriteList: {
        width: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",

    },

    blockTekst:{
        display: "flex",
        flexDirection: "column",
        width:"100%",
       alignItems: "start",
        justifyContent: "space-between",
    },
    favoriteItem: {
        border: "1px solid #081992",
        marginBottom: "4%",
        padding: "2%",
        width: "90%",
      /*  '& .MuiButton-colorInherit': {color:"#fff"}*/

/*        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"*/
    },


    titleText: {
        color: "#081992",
        fontWeight: "bolder",
        paddingLeft: "10px",
        margin: 0
    },
    text: {
        paddingLeft: "10px",
        paddingTop: "10px",
        margin: 0
    },
}))

export interface IProps {
    list: IAdvertItems[]

}

const label = {inputProps: {'aria-label': 'Checkbox demo'}};

const FavoriteList: React.FC<IProps> = ({list}) => {
    const classes = useStyles();
    const [icon, setIcon] = useState("NOT FAV");
    const [checked, setChecked] = useState(false);

    const saveFavorites = (id: string) => {
        // if (checked) {
        //     setIcon("NOT FAV")
        //     console.log("NOT FAV")
        //     setChecked(!checked);
        // }else{
        //     setIcon("FAV")
        //     console.log("FAV")
        //     setChecked(!checked);
        // }

        /* fetch(`http://localhost:8080/annons/${id}`, {
             method: "PUT",
             headers: {
                 Accept: "*!/!*",
             }
         })
             .then(resp => resp.json())
             .then(anons => {
                 console.log(anons)
             })*/
    }


    /* const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setFavorites(event.currentTarget)
         console.log(favorites)
     };*/

    /*  {id: "", role: "", ort, "", yrke: "", fav: false }*/


    function btnHandler(item: IAdvertItems) {
        console.log(item)
    }

    return (
        <div className={classes.favoriteList}>
            {
                list.map((item) => (
                    <div style={{backgroundColor:"#fff", marginBottom:'10px'}} key={item.id} className={classes.favoriteItem}>
                        <div className={classes.blockTekst}>
                            <h3 className={classes.titleText}>
                                {item.name}
                            </h3>
                            <h4 className={classes.text}>{item.description}</h4>
                            <p className={classes.text}>{item.area}</p>
                            <p className={classes.text}>{item.profession}</p>
                        </div>
                        <div>
                            <Button
                                variant="contained"
                                onClick={() => btnHandler(item)}
                            >
                                Sök
                            </Button>
                            {/*<Checkbox {...label} onClick={() => saveFavorites(item.id)}/>*/}
                        </div>
                    </div>
                ))
            }
        </div>
    );
};
export default FavoriteList
