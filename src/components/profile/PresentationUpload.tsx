import * as React from 'react';
import {styled} from '@mui/material/styles';
import Button from '@mui/material/Button';
import {FC} from "react";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    container: {
        border: "2px solid white",
        paddingTop: "20px",
        paddingBottom: "20px",
        '& p, label': {marginLeft: "20px"}
    }
}))

interface I {
    title: string,
    btnTitle: string
}

const Input = styled('input')({
    display: 'none',
});

const PresentationUpload: FC<I> = (props) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <p style={{fontWeight: "bold"}}>{props.title}</p>
            <p>StudentEfternamn-CV.pdf</p>
            <label htmlFor="contained-button-file">
                <Input accept="image/*" id="contained-button-file" multiple type="file"/>
                <Button variant="contained" component="span">
                    {props.btnTitle}
                </Button>
            </label>
        </div>
    );
};

export default PresentationUpload;