import React, {FC} from 'react';
import TextInformationColumn from "./TextInformationColumn";
import {Button} from "@mui/material";


interface I{
    onClick: () => void
}
const TextInformation:FC<I> = (props) => {

    return (
        <div>
            <TextInformationColumn firstColumn={"namn"} secondColumn={"efternamn"}/>
            <TextInformationColumn firstColumn={"Personnummer"} secondColumn={"124124214"}/>
            <TextInformationColumn firstColumn={"E-mail"} secondColumn={"@test"}/>
            <TextInformationColumn firstColumn={"C/o address"} secondColumn={"gatan"}/>
            <TextInformationColumn firstColumn={"Postnummer"} secondColumn={"987 63"}/>
            <TextInformationColumn firstColumn={"Postort"} secondColumn={"Stockholm"}/>
            <TextInformationColumn firstColumn={"Postort"} secondColumn={"Stockholm"}/>
            <TextInformationColumn firstColumn={"Land"} secondColumn={"Sverige"}/>
            <TextInformationColumn firstColumn={"Webisde"} secondColumn={"http://d"}/>
            <div style={{marginTop: "30px", display:"flex",justifyContent:"center"}}>
                <Button
                    variant="contained"
                    onClick={props.onClick}
                >
                    Redigera Uppgifter
                </Button>
            </div>
        </div>
    );
};

export default TextInformation;
