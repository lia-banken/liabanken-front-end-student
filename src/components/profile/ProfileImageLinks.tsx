import React, {FC} from 'react';
import ImageLink from "./ImageLink";

const ProfileImageLinks = () => {
    const onClickHandler = () => {
        console.log(1)
    };

    return (
        <div style={{backgroundColor: "white", border: "solid", width: "1100px"}}>
            <p style={{
                fontWeight: "bold",
                paddingTop: "30px",
                marginLeft: "10px",
            }}>
                Digitala tjänster
            </p>
            <div style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                paddingBottom: "100px",
            }}
            >
                <div style={{marginRight: "10px"}}>
                    <ImageLink onClick={onClickHandler}/>
                </div>
                <div style={{marginLeft: "50px"}}>
                    <ImageLink/>
                </div>
            </div>
        </div>
    );
};

export default ProfileImageLinks;