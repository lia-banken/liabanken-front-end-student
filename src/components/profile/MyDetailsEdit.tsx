import React, {FC} from 'react';
import TextFieldColumn from "./TextFieldColumn";
import {makeStyles} from "@material-ui/core";
import {Button} from "@mui/material";

const useStyles = makeStyles(() => ({
    container: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        '& div': {marginBottom: "5px"}
    }
}))

interface I{
    onClickSave: () => void,
    onClickExit: () => void
}

const MyDetailsEdit:FC<I> = (props) => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <div><TextFieldColumn firstColumn={"Förnamn"} secondColumn={"Efternamn"}/></div>
            <div><TextFieldColumn firstColumn={"E-mail"} secondColumn={"Telefonnummer"}/></div>
            <div><TextFieldColumn firstColumn={"C/o address"} secondColumn={"Hemaddress"}/></div>
            <div><TextFieldColumn firstColumn={"Postnummer"} secondColumn={"Postort"}/></div>
            <div><TextFieldColumn firstColumn={"Land"} secondColumn={"Websida"}/></div>
            <div>
                <Button
                    variant="contained"
                    onClick={props.onClickSave}
                >
                    Spara uppfifter
                </Button>
                <Button
                    variant="outlined"
                    color="error"
                    style={{marginLeft: "10px"}}
                    onClick={props.onClickExit}
                >
                    Avbryt
                </Button>
            </div>


        </div>
    );
};

export default MyDetailsEdit;
