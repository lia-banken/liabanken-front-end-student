import React, {FC} from 'react';
import {Card, CardActionArea, CardContent, CardMedia, Typography} from "@mui/material";
import Imagelinks from '../../assets/ImageLinks.png';

interface I{
    onClick?: () => void
}

const ImageLink:FC<I> = (props) => {
    return (
        <Card
            sx={{ maxWidth: 345, border: "2px solid lightgrey" }}
            onClick={props.onClick}
        >
            <CardActionArea>
                <CardMedia
                    component="img"
                    height="140"
                    image={Imagelinks}
                    alt="green iguana"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Vilka tekniker ska du använda
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default ImageLink;

