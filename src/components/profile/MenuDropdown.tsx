import React, {FC, useEffect, useState} from 'react';
import {Paper} from "@mui/material";

import {Button, makeStyles} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import {IStudent} from "../../interface";
import {useKeycloak} from "@react-keycloak/web";


const useStyles = makeStyles(() => ({
    menuContainer: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        width: "90%",
        marginLeft: "auto",
        marginRight: "auto",
        '@media (min-width: 1000px)': {
            width: "300px"
        }
    },
    paperContainer: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        alignItems: "center",
        justifyItems: "left",
        marginTop: "20px",
    },
    menuList: {
        display: "flex",
        height: "auto",
        width: "100%",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "left",
        backgroundColor: "#fff",
        border: "1px solid",
    },
    headingForNav: {
        display: "flex",
        width: "100%",
        alignItems: "center",
        justifyContent: "left",
        backgroundColor: "#fff",
        border: "1px solid blue",
        paddingTop: "5px",
        paddingBottom: "5px",

        marginBottom: "1%"
    },
    button: {
        display: "flex",
        justifyContent: "left",
        width: "100%",
        textTransform: "none"
    },
    buttonHideMenu: {
        padding: "10px",
        display: "flex",
        justifyContent: "center",
        width: "100%",
        textTransform: "none",
        color: "#efeaea",
        backgroundColor: "#081992",
        boxShadow: "unset",
        border: "none",
        margin: "0",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#4c5499",
        },
        "&:active": {
            boxShadow: "none",
            backgroundColor: "#243592",
        }
    },
}));


const MenuDropdown: FC = () => {
    const classes = useStyles();
    const history = useHistory()
    const [showAndHideMenu, setShowAndHideMenu] = useState<boolean>(false);
    const [student, setStudent] = useState<IStudent>()
    const {keycloak, initialized} = useKeycloak()
    const kcToken = keycloak?.token ?? '';

    const handleShowAndHideMenu = () => {
        setShowAndHideMenu(!showAndHideMenu);

    }
    const onClickToggleMenu = (string: string) => {
        history.push(string);
        handleShowAndHideMenu();
    }
    useEffect(() => {

            const fetchData = async () => {
                if(keycloak.authenticated) {

                    await fetch("http://localhost:8080/student/own", {
                        headers: {
                            "Authorization": "Bearer " + kcToken
                        }
                    })
                }
            };

        const timer = setTimeout(() => {
            fetchData();
        }, 1000);

        return () => clearTimeout(timer);

    }, [keycloak.authenticated, kcToken]);


    return (
        <div>
            <div className={classes.menuContainer}>
                <Paper className={classes.paperContainer}>
                    <div className={classes.headingForNav}>{student?.firstName}</div>
                    {showAndHideMenu && <div className={classes.menuList}>
                        <Button
                            className={classes.button}
                            onClick={() => onClickToggleMenu("/student/myHome")}>Hem
                        </Button>
                        <Button
                            className={classes.button}
                            onClick={() => onClickToggleMenu("/student/myInfo")}>Mina
                            uppgifter</Button>
                        <Button
                            className={classes.button}
                            onClick={() => onClickToggleMenu("/student/myPresentation")}>Mina
                            Presentation</Button>
                        <Button
                            className={classes.button}
                            onClick={() => onClickToggleMenu("/student/myFavorite")}>Mina
                            Favoriter</Button>
                        <Button
                            className={classes.button}
                            onClick={() => onClickToggleMenu("/student/settings")}>Inställningar
                        </Button>
                    </div>}
                    <button className={classes.buttonHideMenu}
                            onClick={() => handleShowAndHideMenu()}>Mina sidor meny
                    </button>
                </Paper>
            </div>
        </div>
    );
}
export default MenuDropdown;
