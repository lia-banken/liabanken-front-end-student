import React, {FC} from 'react';
import {TextField} from "@mui/material";

interface I{
    firstColumn: string,
    secondColumn: string
}
const TextFieldColumn:FC<I> = (props) => {
    return (
        <div style={{display: "flex", flexDirection: "row"}}>
            <TextField
                id="outlined-basic"
                label={props.firstColumn}
                variant="outlined"
                style={{marginRight: "10px"}}
            />
            <TextField
                id="outlined-basic"
                label={props.secondColumn}
                variant="outlined"
                style={{marginLeft: "10px"}}
            />
        </div>
    );
};

export default TextFieldColumn;