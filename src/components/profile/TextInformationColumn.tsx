import React, {FC} from 'react';
import {makeStyles} from "@material-ui/core";


interface I {
    firstColumn: string,
    secondColumn: string,
}

const useStyles = makeStyles(() => ({
    container: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        width: "100%",
        '& p': {marginLeft: "10px"}
    }
}))

const TextInformationColumn: FC<I> = (props) => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <div style={{border: "1px solid lightgrey", width: "130px", backgroundColor: "#EFEFEF"}}>
                <p style={{fontWeight: "bold",}}>
                    {props.firstColumn}
                </p>
            </div>
            <div style={{border: "1px solid lightgrey", width: "200px"}}>
                <p>{props.secondColumn} </p>
            </div>
        </div>
    );
};

export default TextInformationColumn;