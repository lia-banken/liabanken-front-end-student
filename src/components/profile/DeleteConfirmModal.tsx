import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {FC} from "react";

interface I {
    open: boolean,
    onClickCancel: () => void,
    onClickConfirm: () => void,
}

const DeleteConfirmModal: FC<I> = (props) => {
    return (
        <Dialog
            open={props.open}
            onClose={props.onClickCancel}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                Ta Bort Konto
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Är du säker på att du vill ta bort ditt konto? Detta går inte att ångra
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant={"contained"} onClick={props.onClickCancel}>Avbryt</Button>
                <Button variant="outlined" color="error" onClick={props.onClickConfirm}>
                    Ta Bort
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default DeleteConfirmModal;
