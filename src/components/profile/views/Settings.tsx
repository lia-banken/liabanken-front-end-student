import React, {useState} from 'react';
import {makeStyles} from "@material-ui/core";
import {Box, Button, Divider, TextField} from "@mui/material";
import DeleteConfirmModal from "../DeleteConfirmModal";


const useStyles = makeStyles(() => ({
    container: {

        width: '90%',
        height: '400px',
        backgroundColor: '#fff',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '20px',
        marginBottom: '100px'

    },
    passwordContainer: {},
    passwordInput: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    deleteContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
}))
const Settings = () => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);

    const deleteBtn = () => {
        setIsOpen(true)
    };

    const closeModal = () => {
        setIsOpen(false)
    };

    return (
        <div>
            <Box sx={{boxShadow: 1}} className={classes.container}>
                <div className={classes.passwordContainer}>
                    <h3 style={{textAlign: 'center'}}>Kontoinställningar</h3>
                    <Divider/>

                    <div className={classes.passwordInput}>
                        <h5>Ändra lösenord</h5>
                        <TextField
                            style={{marginBottom: '5px'}}
                            id="standard-password-input"
                            label="Password"
                            type="password"
                            autoComplete="current-password"
                            variant="standard"
                        />
                        <TextField
                            style={{marginBottom: '5px'}}
                            id="standard-password-input"
                            label="Nytt lösenord"
                            type="password"
                            autoComplete="current-password"
                            variant="standard"
                            disabled={true}
                        />
                        <Button
                            style={{marginBottom: '20px'}}
                            variant='outlined'>Ändra lösenord</Button>
                    </div>
                    <Divider/>
                </div>
                <div className={classes.deleteContainer}>
                    <h5>Ta bort konto</h5>
                    <Button variant='outlined' onClick={() => deleteBtn()}>Ta bort konto</Button>
                </div>
            </Box>

             <DeleteConfirmModal
                open={isOpen}
                onClickCancel={() => closeModal()}
                onClickConfirm={() => console.log(1)}/>

        </div>
    );
};

export default Settings;
