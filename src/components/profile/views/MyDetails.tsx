import React, {useState} from 'react';
import MenuDropdown from "../MenuDropdown";
import {Box, Divider} from "@mui/material";
import TextInformationColumn from "../TextInformationColumn";
import {makeStyles} from "@material-ui/core";
import TextInformation from "../TextInformation";
import MyDetailsEdit from "../MyDetailsEdit";
import {log} from "util";
import ImageLink from "../ImageLink";
import ProfileImageLinks from "../ProfileImageLinks";

const useStyles = makeStyles(() => ({
    textContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        marginRight: "10px"
    },
    textTop: {
        /*marginLeft: "30px"*/
    },
    textBottom: {
        /*marginLeft: "100px"*/
    },

}))

const MyDetails = () => {
    const classes = useStyles();
    const [showDetails, setShowDetails] = useState(true)
    const [showDetailsEdit, setShowDetailsEdit] = useState(false)

    const toggleViews = () => {
        if (showDetails == true) {
            setShowDetails(false)
            setShowDetailsEdit(true)
        } else if (showDetailsEdit == true) {
            setShowDetailsEdit(false)
            setShowDetails(true)
        }
    };

    return (
        <div style={{backgroundColor: "#EFEFEF"}}>
            <Box sx={{boxShadow: 1}} style={{margin: "20px", backgroundColor: "white", padding: "20px"}}>
                <h3>Mina uppgifter</h3>
                <Divider style={{marginTop: "10px", marginBottom: "10px",}}/>
                {showDetails && <TextInformation onClick={toggleViews}/>}
                {showDetailsEdit && <MyDetailsEdit onClickSave={toggleViews} onClickExit={() => console.log(1)}/>}
            </Box>
            <ProfileImageLinks/>
        </div>
    );
};

export default MyDetails;
