import React, {useEffect, useState} from 'react';

import FavoriteList from "../FavoriteList";
import {IAdvertItems} from "../../../interface";


const MyFavorites = () => {
    const [favoriteData, setFavoriteData] = useState<IAdvertItems[]>([]);
    const [id, setId] = useState<string>('')
   /* const data =
        [
            {id: "1", role: "Program", ort: "Stockhpl.m", yrke: "prgroa", fav: false},
            {id: "2", role: "Progra", ort: "Stockhpl.m", yrke: "prgroa", fav: false},
            {id: "3", role: "Progra", ort: "Stockhpl.m", yrke: "prgroa", fav: false},
            {id: "4", role: "Progrm", ort: "Stockhpl.m", yrke: "prgroa", fav: false},
            {id: "5", role: "Pram", ort: "Stockhpl.m", yrke: "prgroa", fav: false},
        ]*/



        useEffect(() => {
            const fetchData = async () => {
                await fetch(`http://localhost:8080/student/all-favorites/${id}`)
                    .then(resp => resp.json())
                    .then(annons => {
                        setFavoriteData(annons)
                        console.log(annons)
                    })
            };

            const timer = setTimeout(() => {
                fetchData();
            }, 1000);

           return () => clearTimeout(timer);
        }, []);



    return (
        <div>
            <div style={{
                border: "2px solid white",
                backgroundColor: "white",
                margin: "20px",
            }}
            >
                <p style={{fontWeight: "bold", marginLeft: "10px"}}>Mina Favoriter</p>
            </div>
            <FavoriteList list={favoriteData}/>
        </div>
    );
};

export default MyFavorites;
