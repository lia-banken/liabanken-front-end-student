import React from 'react';
import UploadImage from "../UploadImage";
import PresentationUpload from "../PresentationUpload";
import {Divider, makeStyles} from "@material-ui/core";
import {Box} from "@mui/material";
import ProfileImageLinks from "../ProfileImageLinks";

const useStyles = makeStyles(() => ({
    container: {
        backgroundColor: "white",
        margin: "20px",
        '& div': {marginLeft: "0px"},
        '@media (min-width: 1000px)': {
            display: "flex",
            flexDirection: "column",

            justifyContent: "center",
            width: "100%",
        }

    },
    divider: {margin: "10px"}
}))

const MyPresentation = () => {
    const classes = useStyles();

    return (
        <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
            <Box sx={{boxShadow: 1}} className={classes.container}>
                <div style={{margin: "10px"}}>
                    <p style={{fontWeight: "bold"}}>Min presentation</p>
                    <p>För att öka din chans för att få lia plats se till arr du ska ha fyllt i alla fält,
                        cv personligt brev och allt sånt jätte viiktigti för sökande av praktik.
                    </p>
                </div>
                <Divider className={classes.divider}/>
                <div>
                    <UploadImage title={"Profilbild"} btnTitle={"Bifoga CV"}/>
                </div>
                <Divider className={classes.divider}/>
                <PresentationUpload title={"CV"} btnTitle={"Upload"}/>
                <Divider className={classes.divider}/>
                <PresentationUpload title={"Personligt Brev"} btnTitle={"Bifoga Personligt Brev"}/>
                <Divider className={classes.divider}/>
                <div style={{paddingBottom: '20px'}}>
                    <UploadImage title={"Video Presentation"} btnTitle={"Bifoga en video"}/>
                </div>
            </Box>
            <div >
                <ProfileImageLinks/>
            </div >
        </div>

    );
};

export default MyPresentation;
