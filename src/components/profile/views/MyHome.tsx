import React, {FC} from "react";
import {Button, Paper} from "@mui/material";
import {makeStyles} from "@material-ui/core";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles(() => ({
    schoolRequestsContainer: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        width: "100%",
        padding: 0,
        margin: 0,

    },
    paperContainer: {
        display: "flex",
        flexDirection: "column",
        height: '200px',
        width: '80%',
        alignItems: "center",
        justifyContent: "center",
        margin: "100px",

    },
    heading: {
        justifyContent: "center",
        alignItems: "center",
        marginBottom: "2%"
    },
    requestButtonContainer: {
        display: "flex",
        justifyContent: "center",
        padding: "3%"
    },

}));
const MyHome = () => {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div className={classes.schoolRequestsContainer}>
            <Paper className={classes.paperContainer} sx={{borderRadius: "20px"}}>
                <h3 className={classes.heading}>Sök lia plats</h3>
                <div className={classes.requestButtonContainer}>
                    <Button onClick={() => history.push("/student/listPage")}
                            sx={{
                                width: "150px",
                                borderRadius: "30px",
                                backgroundColor: "#421FCE",
                                color: "#efeaea",
                            }} variant="contained">Sök
                    </Button>
                </div>
            </Paper>
        </div>
    );
}
export default MyHome;
