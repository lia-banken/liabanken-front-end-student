import React from 'react';
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    article: {
        border: "1px solid #081992",
        margin: "2%",
        padding: "5%",
        "@media (min-width: 0px)": {
            width: "85%",
        },
        "@media (min-width: 650px)": {
            width: "30%",
        },
        "@media (min-width: 800px)": {
            width: "15%",
        }
        // width:"290px"
    },
    titleText: {
        color: "#081992",
        fontWeight: "bolder",
        padding: 0,
        margin: 0
    },
    text: {
        padding: 0,
        margin: 0
    },


}));

interface IArticle {
    titleText: string;
    text: string;
}

const Article: React.FC<IArticle> = ({titleText, text}) => {
    const classes = useStyles();
    return (
        <div className={classes.article}>
            <p className={classes.titleText}>{titleText}</p>
            <p className={classes.text}>{text}</p>
        </div>
    );
};

export default Article;
