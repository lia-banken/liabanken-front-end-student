import React from 'react';
import {makeStyles} from "@material-ui/core";
import LanguageIcon from '@mui/icons-material/Language';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import StarIcon from '@mui/icons-material/Star';
import FooterItem from "./FooterItem";

const useStyles = makeStyles(() => ({
    footer: {
        width: "100%",
        bottom: 0, //Here is the trick
        backgroundColor: "#081992",
        color: "#efeaea",

    },
    footerElement: {
        display: "flex",

        "@media (max-width: 800px)": {
            padding: "3%",
            width:"100%",
        },
        "@media (min-width: 800px)": {
            width:"50%",
            padding: "1.5%",
        }
    },
    footerTop:{
        //display: "flex",

    },
    footerBottom: {
        display: "flex",
        "@media (max-width: 400px)": {
            justifyContent: "left",
            flexDirection: "column"
        },
        "@media (min-width: 400px)": {
            alignItems: "center",
            justifyContent: "flex-start",
        }
    },
    liabankenAndDescription: {
        display: "flex",
        flexDirection: "column"
    },
    footerFollowUsOn:{
        display: "flex",
        justifyContent: "flex-start",
        "@media (max-width: 800px)": {
            padding: "3%",
            display: "flex",
            width:"100%",
        },
        "@media (min-width: 800px)": {
            padding: "1.5%",
            display: "flex",
            width:"50%",

        }
    }
}));

const Footer: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.footer}>
            <div>
                <div className={classes.footerTop}>
                    <div data-test='footer-languages' className={classes.footerElement}>
                        <LanguageIcon/>
                        <FooterItem   text={"Other Languages"}/>
                    </div>
                    <div data-test='footer-contact-us' className={classes.footerElement}>
                        <LocalPhoneIcon/>
                        <FooterItem    text={"Kontakta oss"}/>
                    </div>
                </div>


                <div className={classes.footerBottom}>
                    <div className={classes.footerElement}>
                        <StarIcon/>
                        <div data-test='footer-bottom-items' className={classes.liabankenAndDescription}>
                            <FooterItem  text={"Liabanken "}/>
                            <FooterItem  text={" Swedish public practic service "}/>
                        </div>
                    </div>
                    <span className={classes.footerFollowUsOn}> Följ oss på Facebook LinkedIn Youtube</span>
                </div>
            </div>
        </div>
    );
};

export default Footer;
