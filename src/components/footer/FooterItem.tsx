import React from 'react';
import {makeStyles} from "@material-ui/core";


const useStyles = makeStyles(() => ({
    footerItem:{
        width:"100%",
        marginLeft:"2%"
    }
}));

interface IFooterItem{
    text:string;

}

const FooterItem:React.FC<IFooterItem> = ({text}) => {
    const classes = useStyles();
    return (
        <span  data-test='view-text-footer-item' className={classes.footerItem}>
            {text}
        </span>
    );
};

export default FooterItem;
