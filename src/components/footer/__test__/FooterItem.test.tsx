import * as ReactDOM from "react-dom";
import FooterItem from "../FooterItem";
import React from "react";
import '@testing-library/jest-dom';


describe('FooterItem component tests', () => {
    let container: HTMLDivElement
    let text:string = "HEJ";
    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container)
        ReactDOM.render(<FooterItem text={text}/>, container)
    })

    afterEach(() => {
        document.body.removeChild(container);
        container.remove();
    })

    test('test render FooterItems correct', () => {
        expect(container.firstElementChild).toBeInTheDocument();
        expect(container.firstElementChild).toContainHTML("HEJ");
    });
    test('test render FooterItems incorrect', () => {
        expect(container.firstElementChild).toBeInTheDocument();
        expect(container.firstElementChild).not.toContainHTML("HELLO");
    });
})