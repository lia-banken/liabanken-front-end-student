import React from 'react';
import * as ReactDOM from 'react-dom';
import Footer from "../Footer";
import '@testing-library/jest-dom';
import FooterItem from "../FooterItem";


describe('Footer component tests', () => {
    let container: HTMLDivElement

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container)
        ReactDOM.render(<Footer/>, container)
    })

    afterEach(() => {
        document.body.removeChild(container);
        container.remove();
    })

    test('test render Footer correct', () => {
        expect(container.querySelector("[data-test='footer-contact-us']"))
           .toBeInTheDocument();
        expect(container.querySelector("[data-test='footer-languages']"))
            .toBeInTheDocument();
        expect(container.querySelector("[data-test='footer-bottom-items']"))
           .toBeInTheDocument();
    });
})
