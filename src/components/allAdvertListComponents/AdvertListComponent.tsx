import React, {useState} from 'react';

import {IAdvertItems} from "../../interface";
import {Checkbox, makeStyles} from "@material-ui/core";
import {Favorite, FavoriteBorder} from "@mui/icons-material";
import {Button, Input} from "@mui/material";
import {useHistory} from "react-router-dom";


const useStyles = makeStyles((theme) => ({
    favoriteList: {
        width: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",

    },

    blockTekst:{
        display: "flex",
        flexDirection: "column",
        width:"100%",
        alignItems: "start",
        justifyContent: "space-between",
    },
    favoriteItem: {
        border: "1px solid #081992",
        marginBottom: "4%",
        padding: "2%",
        width: "90%",
        /*  '& .MuiButton-colorInherit': {color:"#fff"}*/

        /*        display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between"*/
    },


    titleText: {
        color: "#081992",
        fontWeight: "bolder",
        paddingLeft: "10px",
        margin: 0
    },
    text: {
        paddingLeft: "10px",
        paddingTop: "10px",
        margin: 0
    },
}))

export interface IProps {
    list: IAdvertItems[]
    onToggle(id:string):void
    btnGetOneAnons(item:IAdvertItems):void


}

const AdvertListComponent: React.FC<IProps> = ({list,onToggle, btnGetOneAnons}) => {
    const classes = useStyles();
    const history = useHistory();
    const [icon, setIcon] = useState("NOT FAV");
    const [checked, setChecked] = useState(false);




  /*  const btnHandler = (item: IAdvertItems) => {
        console.log(item)
        history.push("advert/" + item.id)
    }*/

    return (
        <div className={classes.favoriteList}>
            {
                list.map((item) => (
                    <Button style={{backgroundColor:"lightgrey", marginBottom:'10px'}}
                            key={item.id}
                            className={classes.favoriteItem}
                            onClick={() => btnGetOneAnons(item)}
                    >
                        <div className={classes.blockTekst}>
                            <h3 className={classes.titleText}>
                                {item.name}
                            </h3>
                            <h4 className={classes.text}>{item.description}</h4>
                            <p className={classes.text}>{item.area}</p>
                            <p className={classes.text}>{item.profession}</p>
                        </div>
                        <div>

                            <Checkbox checked={item.fav}  onClick={() => onToggle(item.id)}/>
                        </div>
                    </Button>
                ))
            }
        </div>
    );
};
export default AdvertListComponent
