import React from 'react';
import {
    Box,
    FormControl,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,

} from "@material-ui/core";



const useStyles = makeStyles((theme) => ({
    block:{
        display:"flex",
        //color:'#FFFFFF',
        width: '40%',

    },
   formControl: {
       backgroundColor:'#FFFFFF',
       width:'180px'

    },

    name:{

        marginLeft:'7px',
        marginTop:'-7px'
    }
}))
const DropDownYrke = () => {
    const classes = useStyles();
    const [stad, setStad] = React.useState('');
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setStad(event.target.value as string);
    };
    return (
        <Box className={classes.block}>
            <FormControl className={classes.formControl}>
                <InputLabel className={classes.name} id="demo-simple-select-label">Yrke</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={stad}
                    label="Yrke"
                    onChange={handleChange}
                >

                    <MenuItem value={''}>None</MenuItem>
                    <MenuItem value={10}>JavaDeveloper</MenuItem>
                    <MenuItem value={20}>WebDeveloper</MenuItem>
                    <MenuItem value={30}>IosDeveloper</MenuItem>
                </Select>

            </FormControl>
        </Box>

    );
};

export default DropDownYrke;
