import React from 'react';
import {
    Box,
    createStyles,
    FormControl,
    InputBase,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,
    Theme,
    withStyles
} from "@material-ui/core";



const useStyles = makeStyles((theme) => ({
    block:{
        display:'flex',
        width: '40%',
        "@media (max-width: 500px)": {
            paddingBottom:"30px"
        }
    },
    formControl: {
        backgroundColor:'#fff',
        width:'180px'
    },

    name:{
        marginLeft:'7px',
        marginTop:'-7px'
    }
}))
const DropDownOrt = () => {
    const classes = useStyles();
    const [stad, setStad] = React.useState('');
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setStad(event.target.value as string);
    };
    return (
        <Box className={classes.block} >
            <FormControl className={classes.formControl}>
                <InputLabel className={classes.name} id="demo-simple-select-label">Ort</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={stad}
                    label="Ort"
                    onChange={handleChange}
                >
                    <MenuItem value={''}>None</MenuItem>
                    <MenuItem value={10}>Stockholm</MenuItem>
                    <MenuItem value={20}>Malmö</MenuItem>
                    <MenuItem value={30}>Örebro</MenuItem>
                </Select>

            </FormControl>
        </Box>
    );
};

export default DropDownOrt;
