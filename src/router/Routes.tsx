import React from 'react';
import {Route, Switch} from "react-router-dom";
import LogInScreen from "../screens/LogInScreen";
import MyPageScreen from "../screens/MyPageScreen";
import AdvertListScreen from "../screens/AdvertListScreen";
import RegistrationOfInterestScreen from "../screens/RegistrationOfInterestScreen";
import LandingScreen from "../screens/LandingScreen";
import SelectAdvertScreen from "../screens/SelectAdvertScreen";

const Routes = () => {
    return (
        <Switch>
            <Route path="/student/login" component={LogInScreen}/>
            {/*<Route path="/student/homePage" component={MyPageScreen}/>*/}
            <Route path="/student/myHome" component={MyPageScreen}/>
            <Route path="/student/advert" component={SelectAdvertScreen} />
            <Route path="/student/myInfo" component={MyPageScreen}/>
            <Route path="/student/myPresentation" component={MyPageScreen}/>
            <Route path="/student/myFavorite" component={MyPageScreen}/>
            <Route path="/student/settings" component={MyPageScreen}/>
            <Route path="/student/listPage" component={AdvertListScreen}/>
            <Route path="/student/registration" component={RegistrationOfInterestScreen}/>
            <Route path="/" component={LandingScreen}/>
        </Switch>
    );
};

export default Routes;
