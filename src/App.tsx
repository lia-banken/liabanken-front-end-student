import React from 'react';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import LandingScreen from "./screens/LandingScreen";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import LogInScreen from "./screens/LogInScreen";
import MyPageScreen from "./screens/MyPageScreen";
import AdvertListScreen from "./screens/AdvertListScreen";
import RegistrationOfInterestScreen from "./screens/RegistrationOfInterestScreen";
import MyDetails from "./components/profile/views/MyDetails";
import MyPresentation from "./components/profile/views/MyPresentation";
import MyFavorites from "./components/profile/views/MyFavorites";
import Settings from "./components/profile/views/Settings";
import MyHome from "./components/profile/views/MyHome";
import Routes from "./router/Routes";
import keycloak from "./keycloak";
import { ReactKeycloakProvider } from '@react-keycloak/web'


function App() {
    return (
        <BrowserRouter>
            <ReactKeycloakProvider  authClient={keycloak}>
                <Header/>
                <Routes/>
                <Footer/>
            </ReactKeycloakProvider>
        </BrowserRouter>
    );
}

export default App;
