export interface IAdvertItems {
    id:string
    name:string
    description:string
    area:string
    profession:string
    fav:boolean
}

export interface IStudent {
    id:string
    firstName:string
    lastName:string
    addressDTO:{
        id:string
        street:string
        zipCode:string
        city: string
    }
    favorites: []
}


